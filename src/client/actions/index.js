export const FETCH_USERS = 'FETCH_USERS';

export const API_PATH = {
  users: '/users'
};

export const fetchUsers = () => async (dispatch, getState, api) => { 
  const users = await api.get(API_PATH.users);
  
  dispatch({
    type: FETCH_USERS,
    payload: users
  })
};