import React, { Component } from 'react';
import {connect} from 'react-redux';
import { fetchUsers } from '../actions';

class UserList extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  renderUsers (user){
    return(
      <li key={user.id}>{user.name}</li>
    )
  };

  render() {
    const { users } = this.props;
    return (
      <div>
        <h1>Users</h1>
        <ul>{ users.map(it => this.renderUsers(it)) }</ul>
      </div>
    )
  }
};

const mapStateToProps = state => ({
  users: state.users
});

const mapDispatchToProps = ({
  fetchUsers
});

export const loadData = (store) => {
  return store.dispatch(fetchUsers());
};

export default { 
  loadData,
  component: connect(mapStateToProps, mapDispatchToProps)(UserList)
};