import React from "react";
import { Link } from "react-router-dom";

const handleClick = () => {
  console.log("hello from client");
};

function Home(props) {
  return (
    <div>
      Home page 2
      <Link to="/hi">Ко второй</Link>
      <button onClick={handleClick}>Click</button>
    </div>
  );
}

export default {
  component: Home
};
